[![Build Status](https://img.shields.io/badge/tests-errors--2%20failures--3-red)](https://git.drupalcode.org/project/drupalsqlsrvci/-/pipelines/3972)
[![Build Status](https://img.shields.io/badge/coverage-94.55-green)](https://git.drupalcode.org/project/drupalsqlsrvci/-/pipelines/3972)

SQL Server Driver for Drupal
=====================

### For Windows or Linux

This contrib module allows the Drupal CMS to connect to Microsoft SQL Server
databases.

## Note on advisory [SA-CORE-2024-008](https://www.drupal.org/sa-core-2024-008)

Drupal versions from [10.3.9]() onwards require users to make a change to their site settings to allow list any third party database module that makes use of the `Drupal\Core\Database\StatementPrefetch` class. __This module does not make use of this class, so no configuration change is required for users of this module__. Should this change in the future, a change notice will be issued. See [#3488910](https://www.drupal.org/project/sqlsrv/issues/3488910) for further information.

Setup
-----

Use [composer](http://getcomposer.org) to install the module:

```bash
$ php composer require drupal/sqlsrv
```

With older versions of Drupal, the driver files needed to be copied to the
directory `/drivers` in the webroot of your Drupal installation. This is no
longer necessary. If you have an older version of this driver in that directory
it should be removed.

If you are upgrading from an older version, the database setup must have the
following keys:
```php
    'namespace' => 'Drupal\\sqlsrv\\Driver\\Database\\sqlsrv',
    'autoload' => 'modules/contrib/sqlsrv/src/Driver/Database/sqlsrv',
    // Use the actual path to the drivers. You may not use 'contrib'.
```

Drupal core allows module developers to use regular expressions within SQL
statements. The base installation does not use this feature, so it is not
required for Drupal to install. However, if any contrib modules use regular
expressions, a CLR will need to be installed that is equivalent to 
`CREATE
   FUNCTION {schema}.REGEXP(@pattern NVARCHAR(100), @matchString NVARCHAR(100))
   RETURNS bit EXTERNAL NAME {name_of_function}`

### Minimum Requirements
 * Drupal 9.3.0
 * SQL Server 2016
 * pdo_sqlsrv 5.10.0

Usage
-----

This driver has a couple peculiarities worth knowing about.

### LIKE expressions

Drupal and the core databases use only two wildcards, `%` and `_`, both of which
are escaped by backslashes. This driver currently uses the default SQL Server
behavior behind-the-scenes, that of escaping the wildcard characters by
enclosing them in brackets `[%]` and `[_]`. When using the `Select::condition()`
function with a LIKE operator, you must use standard Drupal format with
backslash escapes. If you need sqlsrv-specific behavior, you can use
`Select::where()`.
```php
// These two statements are equivalent
$connection->select('test', 't')
  ->condition('t.route', '%[route]%', 'LIKE');
$connection->select('test', 't')
  ->where('t.route LIKE :pattern', [':pattern' => '%[[]route]%']);
```
Note that there is a [PDO bug](https://bugs.php.net/bug.php?id=79276
) that prevents multiple
`field LIKE :placeholder_x ESCAPE '\'` expressions from appearing in one SQL
statement. A different escape character can be chosen if you need a custom
escape character multiple times. This bug just affects the backslash.

### Binary Large Objects

Varbinary data in SQL Server presents two issues. SQL Server is unable to
directly compare a string to a blob without casting the string to varbinary.
This means a query cannot add a ->condition('blob_field', $string) to any
Select query. Thankfully, this is not done in core code, but there is nothing
to stop core from doing so in the future. Contrib modules may currently use
this pattern.

### Non-ASCII strings

Most varchar data is actually stored as nvarchar, because varchar is ascii-only.
Drupal uses UTF-8 while nvarchar encodes data as UCS-2. There are some character
encoding issues that can arise in strange edge cases. Data is typically saved to
varbinary as a stream of UTF-8 characters. If, instead, an nvarchar is converted
into a varbinary, and the binary data extracted into Drupal, it will not be the
same as when it started.

### Collation

The 1.x branch creates the database with a case-insensitive collation for text
fields. However, all non-MySQL databases use case-sensitive default.

### Transactions

The Microsoft ODBC driver, which the PDO driver interfaces with, does
not handle failures within transactions gracefully. If the database produces an
error within a transaction, the entire transaction is automatically rolled back,
even if there are savepoints.

Outstanding Issues
-----
The issues mentioned above means that the sqlsrv driver does not pass every core
test. The project issues queue lists the failing core tests, and the progress in
remedying them.

The following are outstanding core issues that affect the sqlsrv driver.

All Versions (needs work, patch review or awaiting merge):
