<?php

namespace Drupal\Tests\sqlsrv\Kernel;

use Drupal\Component\Utility\Random;
use Drupal\Core\Database\Database;

/**
 * Tests the Upsert query builder.
 *
 * @group Database
 */
class UpsertTest extends SqlsrvTestBase {

  /**
   * Ensures that we can upsert a large number of rows at once.
   */
  public function testMultiBatchUpsert() {
    $connection = Database::getConnection();
    $num_records_before = $connection->query('SELECT COUNT(*) FROM {test_people}')->fetchField();

    $upsert = $connection->upsert('test_people')
      ->key('job')
      ->fields(['job', 'age', 'name']);

    // Add many rows.
    $values = [];
    $number_of_records = 500;
    $random_generator = new Random();
    for ($i = 0; $i < $number_of_records; $i++) {
      $random_name = $random_generator->name();
      $random_word = $random_generator->word(10);
      $random_int = \rand(0, 114);
      $upsert->values([
        'job' => $random_word,
        'age' => $random_int,
        'name' => $random_name,
      ]);
    }

    // Update an existing row.
    $upsert->values([
      'job' => 'Speaker',
      // The initial age was 30.
      'age' => 32,
      'name' => 'Meredith',
    ]);

    $result = $upsert->execute();
    $this->assertIsInt($result);
    $this->assertGreaterThanOrEqual(501, $result, 'The result of the upsert operation should report that at least 501 rows were affected.');

    $num_records_after = $connection->query('SELECT COUNT(*) FROM {test_people}')->fetchField();
    $this->assertEquals($num_records_before + 500, $num_records_after, 'Rows were inserted and updated properly.');

    $person = $connection->query('SELECT * FROM {test_people} WHERE [job] = :job', [':job' => 'Speaker'])->fetch();
    $this->assertEquals('Speaker', $person->job, 'Job was not changed.');
    $this->assertEquals(32, $person->age, 'Age updated correctly.');
    $this->assertEquals('Meredith', $person->name, 'Name was not changed.');
  }

}
