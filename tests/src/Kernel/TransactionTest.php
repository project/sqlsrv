<?php

namespace Drupal\Tests\sqlsrv\Kernel;

use Drupal\Core\Database\TransactionNameNonUniqueException;
use Drupal\Core\Database\TransactionNoActiveException;
use Drupal\Core\Database\TransactionOutOfOrderException;

/**
 * Tests transaction handling.
 *
 * @group Database
 */
class TransactionTest extends SqlsrvTestBase {

  /**
   * Tests transaction rollback.
   *
   * Rollback out of order causes an error.
   *
   * @covers \Drupal\sqlsrv\Driver\Database\sqlsrv\Connection::rollBack
   */
  public function testRollbackOutOfOrder() {
    $txn1 = $this->connection->startTransaction('txn1');
    $txn2 = $this->connection->startTransaction('txn2');
    $txn3 = $this->connection->startTransaction('txn3');
    $this->expectException(TransactionOutOfOrderException::class);
    $txn2->rollback();
  }

  /**
   * Tests transaction rollback.
   *
   * Rollback a previously rolled back transaction causes an error.
   *
   * @covers \Drupal\sqlsrv\Driver\Database\sqlsrv\Connection::rollBack
   */
  public function testRollbackTransactionName() {
    $txn1 = $this->connection->startTransaction('txn1');
    $txn2 = $this->connection->startTransaction('txn2');
    $txn2->rollback();
    $this->expectException(TransactionNoActiveException::class);
    // Try to roll it back again.
    $txn2->rollback();
  }

  /**
   * Tests transaction must have unique names.
   *
   * @covers \Drupal\sqlsrv\Driver\Database\sqlsrv\Connection::pushTransaction
   */
  public function testDuplicateTransactionName() {
    $txn1 = $this->connection->startTransaction('txn1');
    $txn2 = $this->connection->startTransaction('txn2');
    $this->expectException(TransactionNameNonUniqueException::class);
    $txn3 = $this->connection->startTransaction('txn2');
  }

}
