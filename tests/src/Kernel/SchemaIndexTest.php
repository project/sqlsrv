<?php

namespace Drupal\Tests\sqlsrv\Kernel;

use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Tests table index management via the schema API.
 *
 * @group Database
 */
class SchemaIndexTest extends SqlsrvTestBase {

  /**
   * Test that we can insert values into a large field with an index.
   *
   * @dataProvider dataProviderForTestLargeIndex
   */
  public function testLargeNonclusteredIndex($field_size, array $index, $index_exists) {
    $table_schema = [
      'description' => 'The base table for aggregator_feed entities.',
      'primary key' => ['id'],
      'indexes' => ['index_test_field__url' => $index],
      'fields' => [
        'id' => [
          'type' => 'serial',
          'unsigned' => TRUE,
          'size' => 'normal',
          'not null' => TRUE,
        ],
        'url' => [
          'type' => 'varchar',
          'length' => $field_size,
          'not null' => TRUE,
        ],
      ],
    ];
    /** @var \Drupal\sqlsrv\Driver\Database\sqlsrv\Schema $schema */
    $schema = $this->connection->schema();
    $schema->createTable('index_test', $table_schema);
    $does_index_exist = $schema->indexExists('index_test', 'index_test_field__url');
    $this->assertSame($index_exists, $does_index_exist);
    // Use a 3 byte character.
    $large_value = str_repeat('€', $field_size);
    $insert = $this->connection->insert('index_test');
    $result = $insert->fields(['url' => $large_value])->execute();
    if (!$index_exists) {
      $index_sql = "CREATE INDEX index_test_field__url_idx ON {index_test} (url)";
      // We should see an exception due to the existing large value.
      $this->expectException(DatabaseExceptionWrapper::class);
      // Create the index.
      $this->connection->query($index_sql);
    }
  }

  /**
   * Provides data for testLargeIndex().
   */
  public function dataProviderForTestLargeIndex() {
    return [
      [
        2048,
        ['url'],
        FALSE,
      ],
      [
        2048,
        [['url', 255]],
        FALSE,
      ],
      [
        100,
        ['url'],
        TRUE,
      ],
      [
        566,
        ['url'],
        TRUE,
      ],
      [
        565,
        ['url'],
        TRUE,
      ],
      [
        567,
        ['url'],
        FALSE,
      ],
    ];
  }

  /**
   * Test that we can insert values into a large field with a multicolumn index.
   *
   * @dataProvider dataProviderForTestLargeMultiIndex
   */
  public function testLargeNonclusteredMultiIndex($field_size, array $index, $index_exists) {
    $table_schema = [
      'description' => 'The base table for aggregator_feed entities.',
      'primary key' => ['id'],
      'indexes' => ['index_test_field__url' => $index],
      'fields' => [
        'id' => [
          'type' => 'serial',
          'unsigned' => TRUE,
          'size' => 'normal',
          'not null' => TRUE,
        ],
        'name' => [
          'type' => 'varchar',
          'length' => 100,
          'unique' => TRUE,
        ],
        'url' => [
          'type' => 'varchar',
          'length' => $field_size,
          'not null' => TRUE,
        ],
      ],
    ];
    /** @var \Drupal\sqlsrv\Driver\Database\sqlsrv\Schema $schema */
    $schema = $this->connection->schema();
    $schema->createTable('index_test', $table_schema);
    $does_index_exist = $schema->indexExists('index_test', 'index_test_field__url');
    $this->assertSame($index_exists, $does_index_exist);
    // Use a 3 byte character.
    $large_value = str_repeat('€', $field_size);
    $name = str_repeat('€', 100);
    $insert = $this->connection->insert('index_test');
    $result = $insert->fields([
      'url' => $large_value,
      'name' => $name,
    ])->execute();
    if (!$index_exists) {
      $index_sql = "CREATE INDEX index_test_field__url_idx ON {index_test} (url, name)";
      // We should see an exception due to the existing large value.
      $this->expectException(DatabaseExceptionWrapper::class);
      // Create the index.
      $this->connection->query($index_sql);
    }
  }

  /**
   * Provides data for testLargeMultiIndex().
   */
  public function dataProviderForTestLargeMultiIndex() {
    return [
      [
        2048,
        ['url', 'name'],
        FALSE,
      ],
      [
        2048,
        [['url', 255], 'name'],
        FALSE,
      ],
      [
        100,
        ['url', 'name'],
        TRUE,
      ],
      [
        465,
        ['url', 'name'],
        TRUE,
      ],
      [
        466,
        ['url', 'name'],
        TRUE,
      ],
      [
        467,
        ['url', 'name'],
        FALSE,
      ],
    ];
  }

}
