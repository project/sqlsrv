<?php

namespace Drupal\Tests\sqlsrv\Kernel;

class NewDatabaseTest extends SqlsrvTestBase {

  /**
   * The sqlsrv schema.
   *
   * @var \Drupal\sqlsrv\Driver\Database\sqlsrv\Schema
   */
  protected $schema;

  /**
   * @inheritdoc
   */
  public function setUp(): void {
    $original = \getenv('SIMPLETEST_DB');
    \putenv('SIMPLETEST_DB=sqlsrv://sa:Password12!@localhost/newdrupalsite?schema=dbo&cache_schema=true&module=sqlsrv');
    parent::setUp();

    /** @var \Drupal\sqlsrv\Driver\Database\sqlsrv\Schema $schema */
    $schema = $this->connection->schema();
    $this->schema = $schema;
  }

  public function testDatabaseName() {
    $options = $this->connection->getConnectionOptions();
    $db = $options['database'];
    $this->assertEquals($db, 'newdrupalsite');
  }

  /**
   * Test the collation of a new database.
   *
   * @covers \Drupal\sqlsrv\Driver\Database\sqlsrv\Schema::getCollation
   */
  public function testCollation() {
    $collation = $this->schema->getCollation();
    $this->assertStringContainsString('CI', $collation);
  }

}
