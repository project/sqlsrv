<?php

namespace Drupal\Tests\sqlsrv\Kernel;

use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Tests\migrate_drupal\Kernel\MigrateDrupalTestBase;

/**
 * Test stub creation for aggregator feeds and items.
 *
 * @group aggregator
 */
class MigrateAggregatorStubTest extends MigrateDrupalTestBase {

  use StubTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['aggregator'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('aggregator_feed');
    $this->installEntitySchema('aggregator_item');

    /** @var \Drupal\sqlsrv\Driver\Database\sqlsrv\Connection $connection */
    $connection = Database::getConnection();
    $connection->queryDirect('CREATE INDEX [aggregator_feed_field__url_idx] ON {aggregator_feed} ([url])');
  }

  /**
   * Tests the correct exception is thrown on nested transactions.
   *
   * @covers \Drupal\sqlsrv\Driver\Database\sqlsrv\Connection::rollback
   */
  public function testItemStub() {
    $this->expectException(EntityStorageException::class);
    $this->expectExceptionMessage("the index 'aggregator_feed_field__url_idx' exceeds the maximum length of");
    $this->performStubTest('aggregator_item');
  }

}
