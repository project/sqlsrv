<?php

namespace Drupal\Tests\sqlsrv\Kernel;

/**
 * Test Connection::nextId()
 *
 * @group Database
 */
class NextIdTest extends SqlsrvTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system'];

  /**
   * {@inheritdoc}
   *
   * Skip any kernel tests if not running on the correct database.
   */
  protected function setup(): void {
    parent::setup();
    $this->installSchema('system', ['sequences']);
  }

  /**
   * Test that Connection::nextId works as expected.
   */
  public function testNextId() {
    $id = $this->connection->nextId();
    $this->assertGreaterThan(0, $id);
    $newid = $this->connection->nextId();
    $this->assertGreaterThan($id, $newid);
    $id = $newid;
    $newid = $this->connection->nextId(100);
    $this->assertGreaterThan(max(100, $id), $newid);
    $id = $newid;
    $newid = $this->connection->nextId(100);
    $this->assertGreaterThan($id, $newid);
    $id = $newid;
    $newid = $this->connection->nextId(20);
    $this->assertGreaterThan($id, $newid);
    $id = $newid;
    $newid = $this->connection->nextId();
    $this->assertGreaterThan($id, $newid);
    $id = $newid;
    $newid = $this->connection->nextId(200);
    $this->assertGreaterThan(max(200, $id), $newid);
    $id = $newid;
    $newid = $this->connection->nextId(202);
    $this->assertGreaterThan(max(202, $id), $newid);
  }

}
