<?php

namespace Drupal\Tests\sqlsrv\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests that the module help path exists and functions.
 *
 * @group Database
 */
class SqlsrvTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['help', 'system', 'sqlsrv'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create and log in user.
    $account = $this->drupalCreateUser([
      'access administration pages',
      'view the administration theme',
      'administer permissions',
      'administer site configuration',
    ]);
    $this->drupalLogin($account);
  }

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test the module help page.
   *
   * @covers ::sqlsrv_help
   */
  public function testHelp() {
    $this->drupalGet('admin/help/sqlsrv');
    $this->assertSession()->pageTextContains('The SQL Server module provides the connection');
  }

  /**
   * Test the module help page.
   *
   * @covers ::sqlsrv_help
   */
  public function testRequirements() {
    $this->drupalGet('admin/reports/status');
    $this->assertSession()->pageTextContains('MSSQL Server client buffer size');
  }

}
